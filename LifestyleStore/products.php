<?php
    session_start();
	require 'connection.php';
    require 'check_if_added.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="img/Logo.gif" />
        <title>TheCraftKart Store</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- latest compiled and minified CSS -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css">
        <!-- jquery library -->
        <script type="text/javascript" src="bootstrap/js/jquery-3.2.1.min.js"></script>		
        <!-- Latest compiled and minified javascript -->
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
        <!-- External CSS -->
        <link rel="stylesheet" href="css/style.css" type="text/css">
    </head>
    <body>
        <div>
            <?php
			    if( (!isset($_SESSION['user_type']) || 'customer' == $_SESSION['user_type']) ){
					require 'header.php';
				}else{
					require 'admin_header.php';
				}
                
            ?>
			
            <div class="container">
                <div class="jumbotron">
                    <h2>Welcome to TheCraftKart!</h2>
                    <p>The Most Authentic and Traditional Handcraft Boutique.</p>
                </div>
				<div style="margin-bottom:30px;margin-left:30px; text-align: center;">
					<form  method="post">
							   <label for="site-search">Search Item By Catagory Name:</label>
								<input type="search" id="site-search" name="search-text"
									   aria-label="Search through site content">

								<button style="background-color:#337ab7;" name="button1">Search</button>
					</form>
				</div>
            </div>
			
			
			<?php
      
				if(isset($_POST['button1'])) {
					$search_text = $_POST['search-text'];					
			
			?>
			
			
			<div class="container">
			
				 <?php 
						$admin_products_query='select id,name,price ,description,item_img_url from items where item_type like \'%'.$search_text.'%\'';
                        $user_products_result=mysqli_query($con,$admin_products_query) or die(mysqli_error($con));
                        $no_of_user_products= mysqli_num_rows($user_products_result);
                        $counter=1;
                       while($row=mysqli_fetch_array($user_products_result)){			   
						   
                           
                ?>
				
				<div class="col-md-3 col-sm-6">
                        <div class="thumbnail">
                            <!--<a href="cart.php">
                                <img src="img/cannon_eos.jpg" alt="Cannon">
                            </a>-->
							<img src="<?php echo $row['item_img_url']?>" alt="Cannon" style="height:250px; width:250px;" >
                            <center>
                                <div class="caption">
                                    <h3><?php echo $row['name']?></h3>
                                    <p>Price: Rs. <?php echo $row['price']?></p>
                                    <p><a href="login.php" role="button" class="btn btn-primary btn-block">Buy Now</a></p>                                       
                                    
                                </div>
                            </center>
                        </div>
                  </div>
				<?php } ?>
			
			</div>
			
			<?php
      
				}else{
			
			?>
			
			<div class="container">
			
				 <?php 
						$admin_products_query='select id,name,price ,description,item_img_url from items ';
                        $user_products_result=mysqli_query($con,$admin_products_query) or die(mysqli_error($con));
                        $no_of_user_products= mysqli_num_rows($user_products_result);
                       
                       while($row=mysqli_fetch_array($user_products_result)){			   
						   
                           
                ?>
				
				<div class="col-md-3 col-sm-6">
                        <div class="thumbnail">
                            <!--<a href="cart.php">
                                <img src="img/cannon_eos.jpg" alt="Cannon">
                            </a>-->
							<img src="<?php echo $row['item_img_url']?>" alt="Cannon" style="height:250px; width:250px;" >
                            <center>
                                <div class="caption">
                                    <h3><?php echo $row['name']?></h3>
                                    <p>Price: Rs. <?php echo $row['price']?></p>
                                    <p><a href="login.php" role="button" class="btn btn-primary btn-block">Buy Now</a></p>                                       
                                    
                                </div>
                            </center>
                        </div>
                  </div>
				<?php } ?>
			
			</div>
			<?php } ?>
			
            <br><br><br><br><br><br><br><br>
           <footer class="footer">
               <div class="container">
               <center>
                   <p>Copyright TheCraftKart Store. All Rights Reserved. | Contact Us: +91 9230325699</p>
                   <p>Asha Majumder</p>
               </center>
               </div>
           </footer>
        </div>
    </body>
</html>
