<nav class="navbar navbar-inverse navabar-fixed-top navbar-light">
               <div class="container">
                   <div class="navbar-header">
				   
                       <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                       </button>
					  
					   
					   <a class="navbar-brand" href="index.php">
						<span><img src="img/Logo.jpg" alt="logo" style="width:40px;"></span>	
							TheCraftKart Store						
					  </a>
					  
					 
					   
                   </div>
				   
				   <!--<div class="collapse navbar-collapse" id="myNavbar1">
                       <ul class="nav navbar-nav navbar-left">
					   <li><img src="img/Logo.gif" alt="Camera" width="35" height="35"></li>
                       <li><a href="index.php" class="navbar-brand">TheCraftKart Store</a></li>
					    </ul>
                   </div>-->
                   
                   <div class="collapse navbar-collapse" id="myNavbar">
                       <ul class="nav navbar-nav navbar-right">
                           <?php
                           if(isset($_SESSION['email'])){
							   if('customer' == $_SESSION['user_type']){
                           ?>
                           <!--<li><a href="cart.php"><span class="glyphicon glyphicon-shopping-cart"></span> Cart</a></li>-->
						   <!--<form action="product_search_script.php">
							   <li><input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="search"></li>
								<li><button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button></li>
						   </form>-->
                           <li><a href="settings.php" ><span class="glyphicon glyphicon-cog"></span> Settings</a></li>
                           <li><a href="logout_script.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
							<?php
							    }else{
							?>
								<li><a href="admin_product.php"><span class="glyphicon glyphicon-shopping-cart"></span> Products</a></li>
								<li><a href="product_add.php"> Add New Product</a></li>
								<li><a href="settings.php"><span class="glyphicon glyphicon-cog"></span> Settings</a></li>
								<li><a href="logout_script.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
								
                           <?php
                           }
						   }else{
                            ?>
							<!--<form action="product_search_script.php">
							   <li><input class="form-control mr-sm-2" type="search" placeholder="Search" name="search"></li>
								<li><button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button></li>
						   </form>-->
                           <li><a href="signup.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                           <li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                           <?php
                           }
                           ?>
                           
                       </ul>
                   </div>
               </div>
</nav>